;
; published as "Enfolding Mists" by Triangulator
; on Literally Nervous Records (NERVOUS-021) in 2017
;

  sr        =  44100
  ksmps     =  32
  nchnls    =  2
  0dbfs     =  1

    instr 1
  iamp      =  p5
  kcps      =  cpspch(p6)
  kcar      =  1
  kmod      =  p4
  kndx      line      0, p3, 20                   

  kmaxvar   =  0.1
  imode     =  1
  iminfrq   =  100
  iprd      =  0.02

  aenv1     madsr     2.2, 0, 1, 6.8
  aenv2     madsr     4.2, 0, .8, 12.8

  kbeta     linseg    0, p3/4, 2, p3/4, 0, p3*0.1, 2, p3*0.15, 0
            seed      20170924
  anoise    fractalnoise  0.05, kbeta 
  
  asig1     foscil    iamp/7, kcps, kcar, kmod, kndx, 1
  asig2     foscil    iamp/13, kcps*1.12, kcar, kmod*1.02, kndx, 3
  asig3     foscil    iamp/19, kcps*1.25, kcar, kmod*1.05, kndx, 2
  asig4     foscil    iamp/25, kcps*1.58, kcar, kmod*1.08, kndx, 4
  asig5     foscil    iamp/19, kcps*1.71, kcar, kmod*1.11, kndx, 2
  asig6     foscil    iamp/13, kcps*1.92, kcar, kmod*1.32, kndx, 5
  asig7     foscil    iamp/7, kcps*2.32, kcar, kmod*1.62, kndx, 1
  asigs     =  asig1 + asig2 + asig3 + asig4 + asig5 + asig6 + asig7

  asig      harmon    asigs, kcps, kmaxvar, kcps*.5, kcps*4, imode, iminfrq, iprd
            outs      (anoise-(asig + asigs)*aenv1*.1)*aenv2 + (asig + asigs)*aenv1*.5, (anoise-(asig + asigs)*aenv1*.1)*aenv2 + (asig + asigs)*aenv1*.4
    endin

    instr 2
  kamp      =  p5
  kcps      =  cpspch(p6)
  isprd     =  p4

  kmaxvar   =  0.6
  imode     =  0
  iminfrq   =  100
  iprd      =  0.02

  aenv      madsr     .3, 0, 1, .8

  asig1     oscil     kamp/6, kcps, 1
  asig2     oscil     kamp/8, kcps*(isprd + isprd -1), 2
  asig3     oscil     kamp/11, kcps*(isprd^2 + isprd -1), 3
  asig4     oscil     kamp/15, kcps*(isprd^3 + isprd -1), 4
  asig5     oscil     kamp/20, kcps*(isprd^4 + isprd -1), 5
  asig6     oscil     kamp/24, kcps*(isprd^5 + isprd -1), 4
  asig7     oscil     kamp/27, kcps*(isprd^6 + isprd -1), 3
  asig8     oscil     kamp/29, kcps*(isprd^7 + isprd -1), 2
  asig9     oscil     kamp/30, kcps*(isprd^8 + isprd -1), 1
  asigs     =  asig1 + asig2 + asig3 + asig4 + asig5 + asig6 + asig7 + asig8 + asig9

  asig      harmon    asigs, kcps, kmaxvar, kcps*.5, kcps*4, imode, iminfrq, iprd
            outs      (asigs + 0.6 * asig)*aenv, (asigs + 0.6 * asig)*aenv
    endin

    instr 3
  iolaps    =  2
  istr      =  p4  /* timescale */
  ipitch    =  p5 /* pitchscale */
  igrsize   =  p6
  iamp      =  p7
  ifreq     =  iolaps/igrsize
  ips       =  1/iolaps

  aenv      madsr     1.2, 0, 1, 8.8

  asig      diskgrain   "enfolding_mists_input.ogg", 1, ifreq, ipitch, igrsize, ips*istr, 6, iolaps
            outs      asig*aenv*iamp, asig*aenv*iamp
    endin
